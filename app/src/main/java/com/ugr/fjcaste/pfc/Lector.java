package com.ugr.fjcaste.pfc;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidplot.util.Redrawer;
import com.androidplot.xy.AdvancedLineAndPointRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;



public class Lector extends ActionBarActivity {

    private BluetoothAdapter bAdapter;
    private BluetoothService bService;
    private BluetoothDevice dispositivo;

    //Variables para estructurar la comunicación
    private String received;

    private ArrayList<Double> tempMeasures;
    private ArrayList<Double> humMeasures;
    private ArrayList<Double> lightMeasures;

    private ArrayList<String[]> csvMeasures;

    private String selectedView = "temperature";

    private Button testButton;
    private Button exportBtn;
    private TextView message;
    private RelativeLayout tempButton;
    private TextView temperature;
    private RelativeLayout humButton;
    private TextView humidity;
    private RelativeLayout lumButton;
    private TextView luminosity;

    private XYPlot plot;
    private ECGModel ecgSeries;

    static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL = 10;

    /**
     * Uses a separate thread to modulate redraw frequency.
     */
    private Redrawer redrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lector);

        //Handler handler = new Handler();
        bAdapter = BluetoothAdapter.getDefaultAdapter();
        bService = new BluetoothService(handler, bAdapter);

        final String direccion;

        //Recuperamos la información pasada en el intent
        Bundle bundle = this.getIntent().getExtras();
        direccion = bundle.getString(MainActivity.EXTRA_ADDRESS);

        conectarDispositivo(direccion);
        message = (TextView) findViewById(R.id.message);
        tempButton = (RelativeLayout) findViewById(R.id.tempButton);
        tempButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.setRangeBoundaries(-10, 50, BoundaryMode.FIXED);
                plot.setTitle("temperatura en la sala");
                ecgSeries.loadData(tempMeasures);
                selectedView = "temperature";
            }
        });
        temperature = (TextView) tempButton.findViewById(R.id.temperature);

        humButton = (RelativeLayout) findViewById(R.id.humidityButton);
        humButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.setRangeBoundaries(0, 100, BoundaryMode.FIXED);
                plot.setTitle("Humedad en la sala");
                ecgSeries.loadData(humMeasures);
                selectedView = "humidity";
            }
        });
        humidity = (TextView) humButton.findViewById(R.id.humidity);

        lumButton = (RelativeLayout) findViewById(R.id.lightButton);
        lumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plot.setRangeBoundaries(0, 100, BoundaryMode.FIXED);
                plot.setTitle("Luminosidad en la sala");
                ecgSeries.loadData(lightMeasures);
                selectedView = "luminosity";
            }
        });
        luminosity = (TextView) lumButton.findViewById(R.id.luminosity);

        tempMeasures = new ArrayList<>();
        lightMeasures = new ArrayList<>();
        humMeasures = new ArrayList<>();
        csvMeasures = new ArrayList<>();


        testButton = (Button) findViewById(R.id.conectarLector);
        testButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bService.enviar("a".getBytes());
            }
        });

        exportBtn = (Button) findViewById(R.id.export);
        exportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkExportEnabled();
            }
        });


        plot = (XYPlot) findViewById(R.id.plot);
        plot.setTitle("Termometro de la sala");
        ecgSeries = new ECGModel(200);
//
//        // add a new series' to the xyplot:
        MyFadeFormatter formatter =new MyFadeFormatter(240);
        formatter.setLegendIconEnabled(false);
        plot.addSeries(ecgSeries, formatter);
        plot.setRangeBoundaries(-10, 50, BoundaryMode.FIXED);
        plot.setDomainBoundaries(0, 100, BoundaryMode.FIXED);

        // reduce the number of range labels
        plot.setLinesPerRangeLabel(3);

        // start generating ecg data in the background:
        ecgSeries.start(new WeakReference<>(plot.getRenderer(AdvancedLineAndPointRenderer.class)));

        // set a redraw rate of 30hz and start immediately:
        redrawer = new Redrawer(plot, 0.5f, true);

    }


    /**
     * Special {@link AdvancedLineAndPointRenderer.Formatter} that draws a line
     * that fades over time.  Designed to be used in conjunction with a circular buffer model.
     */
    public static class MyFadeFormatter extends AdvancedLineAndPointRenderer.Formatter {

        private int trailSize;

        public MyFadeFormatter(int trailSize) {
            this.trailSize = trailSize;
        }

        @Override
        public Paint getLinePaint(int thisIndex, int latestIndex, int seriesSize) {
            // offset from the latest index:
            int offset;
            if(thisIndex > latestIndex) {
                offset = latestIndex + (seriesSize - thisIndex);
            } else {
                offset =  latestIndex - thisIndex;
            }

            float scale = 255f / trailSize;
            int alpha = (int) (255 - (offset * scale));
            //getLinePaint().setAlpha(alpha > 0 ? alpha : 0);
            return getLinePaint();
        }
    }


    /**
     * Primitive simulation of some kind of signal.  For this example,
     * we'll pretend its an ecg.  This class represents the data as a circular buffer;
     * data is added sequentially from left to right.  When the end of the buffer is reached,
     * i is reset back to 0 and simulated sampling continues.
     */
    public static class ECGModel implements XYSeries {

        private final Number[] data;
        private boolean keepRunning;
        private int latestIndex;

        private WeakReference<AdvancedLineAndPointRenderer> rendererRef;

        /**
         *
         * @param size Sample size contained within this model
         */
        public ECGModel(int size) {
            data = new Number[size];
            for(int i = 0; i < data.length; i++) {
                data[i] = 0;
            }
        }

        public void start(final WeakReference<AdvancedLineAndPointRenderer> rendererRef) {
            this.rendererRef = rendererRef;
        }

        @Override
        public int size() {
            return data.length;
        }

        @Override
        public Number getX(int index) {
            return index;
        }

        @Override
        public Number getY(int index) {
            return data[index];
        }

        @Override
        public String getTitle() {
            return "Signal";
        }

        public void setNextData(double tempData){

            if (latestIndex >= data.length) {
                latestIndex = 0;
            }

            data[latestIndex] = tempData;

            if(latestIndex < data.length - 1) {
                // null out the point immediately following i, to disable
                // connecting i and i+1 with a line:
                data[latestIndex +1] = null;
            }
            latestIndex++;

            Log.d("PFC", "Recibido: " + tempData);
        }

        public void loadData(ArrayList<Double> newData){
            for(int i = 0; i < data.length; i++) {
                data[i] = 0;
            }

            latestIndex = 0;
            for(int i = 0; i < newData.size(); i++) {
                setNextData(newData.get(i));
            }
        }
    }

    private void checkExportEnabled() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL);
        } else {
            exportCsv();
        }

    }

    public void exportCsv() {
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        String fileName = "ExportMeasures_PFC.csv";
        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath );
        CSVWriter writer;
        try {
            // File exist
            if(f.exists() && !f.isDirectory()){
                FileWriter mFileWriter = new FileWriter(filePath , true);
                writer = new CSVWriter(mFileWriter);
            }
            else {
                writer = new CSVWriter(new FileWriter(filePath));
                String[] header = {"Timestamp", "Temperature", "Humidity", "Luminosity"};
                writer.writeNext(header);
            }

            writer.writeAll(csvMeasures);
            Log.d("PFC", "Export completed");
            writer.close();
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public void conectarDispositivo(String direccion) {
        //Toast.makeText(this, "Conectando a " + direccion, Toast.LENGTH_LONG).show();
        if (bService != null) {
            BluetoothDevice dispositivoRemoto = bAdapter.getRemoteDevice(direccion);
            bService.solicitarConexion(dispositivoRemoto);
            dispositivo = dispositivoRemoto;
        } else {
            Log.d("PFC", "Conexion error");
        }


    }

    private final Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            byte[] buffer = null;
            String mensaje = null;

            //message.setText("");
            // Atendemos al tipo de mensaje
            switch (msg.what) {
                // Mensaje de lectura: se mostrara en el TextView
                case BluetoothService.MSG_RECIBIR: {
                    // Leemos del flujo de entrada del socket
                    buffer = (byte[])msg.obj;
                    mensaje = new String(buffer, 0, msg.arg1);
                    received = received + mensaje;

                    String[] btdata = received.split("e");

                    if(btdata.length > 1) {

                        //Log.d("PFC", "Datos " + btdata[0]);
                        if(btdata[0].length() == 14){
                            try {
                                double tempValue = Double.parseDouble( btdata[0].substring(0,5));
                                tempMeasures.add(tempValue);
                                temperature.setText("" + Math.round(tempValue) + "ºC");

                                double humidityValue = Double.parseDouble( btdata[0].substring(5,10));
                                humMeasures.add(humidityValue);
                                humidity.setText("" + Math.round(humidityValue) + "%");

                                double luminosityValue = Double.parseDouble( btdata[0].substring(10,14));
                                luminosityValue = 100 - Math.round(luminosityValue / 5 * 100);
                                lightMeasures.add(luminosityValue);
                                luminosity.setText("" + luminosityValue + "%");

                                switch (selectedView){
                                    case "luminosity":
                                        ecgSeries.setNextData(luminosityValue);
                                        break;
                                    case "temperature":
                                        ecgSeries.setNextData(tempValue);
                                        break;
                                    case "humidity":
                                        ecgSeries.setNextData(humidityValue);
                                        break;
                                }

                                Date currentTime = Calendar.getInstance().getTime();
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
                                String [] receivedMeasure = {
                                        simpleDateFormat.format(currentTime),
                                        String.valueOf(tempValue),
                                        String.valueOf(humidityValue),
                                        String.valueOf(luminosityValue)
                                };
                                csvMeasures.add(receivedMeasure);

                            } catch (NumberFormatException numEx){}
                        }


                        received = btdata[btdata.length -1];
                    }
                }
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    exportCsv();
                }
                return;
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        redrawer.finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        redrawer = new Redrawer(plot, 0.5f, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_lector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}